from datetime import datetime
from flask import make_response, abort


def get_timestamp():
    return datetime.now().strftime(('%Y-%m-%d %H:%M:%S'))

people = {
    "Farrell": {
        "fname": "Doug",
        "lname": "Farrell",
        "timestamp": get_timestamp()
    },
    "Brockman": {
        "fname": "Kent",
        "lname": "Brockman",
        "timestamp": get_timestamp()
    },
    "Easter": {
        "fname": "Bunny",
        "lname": "Easter",
        "timestamp": get_timestamp()
    }
}

def read():
    return [people[key] for key in sorted(people.keys())]

def check():
    return [key for key in sorted(people.keys())]

def create(person):
    lname = person.get('lname', None)
    fname = person.get('fname', None)

    if fname not in people and lname is not None:
        people[lname] = {
            'lname': lname,
            'fname': fname,
            'timestamp': get_timestamp()
        }
        return make_response(
            "{} created".format(lname), 201
        )
    else:
        abort(406, 'Person with {} exists'.format(lname))

def update(lname, person):
    if lname in people:
        people[lname]["fname"] = person.get("fname")
        people[lname]["timestamp"] = get_timestamp()

        return people[lname]
    else:
        abort(
            404, 'Person {} not found'.format(lname)
        )
def delete(lname):
    if lname in people:
        del people[lname]

        return make_response(
            '{} deleted'.format(lname), 200
        )
    else:
        abort(
            404, '{} not found'.format(lname)
        )
