people = {
    "Farrell": {
        "fname": "Doug",
        "lname": "Farrell",
    },
    "Brockman": {
        "fname": "Kent",
        "lname": "Brockman",
    },
    "Easter": {
        "fname": "Bunny",
        "lname": "Easter",
    }
}

k = [i for i in people.keys()]
lname = 'Farrell'

if lname in people:
    people[lname]['lname'] = 'New'

print(people)
